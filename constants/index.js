// состояния товарных позиций
export const PRODUCT_CONDITIONS = {
  BRAND_NEW: "brand_new", // новое с биркой
  EXCELLENT: "excellent", // отличное
  NORMAL: "normal", // нормальное
  TRASH: "trash", // мусор
};

// статусы товарных позиций
export const PRODUCT_STATUSES = {
  DRAFT: "draft", // черновик
  PREPARING: "preparing", // подготовка
  AUTHENTICATION: "authentication", // аутентификация
  ON_PHOTO: "on_photo", // на фотографировании
  CHECKING: "checking", // проверка
  STOCK_READY: "stock_ready", // готов к продаже
  IN_STOCK: "in_stock", // в продаже
  SOLD: "sold", // продан
  DECLINED: "declined", // отказано
  RETURNED: "returned", // возвращено владельцу
  REJECTED: "rejected", // снят с продажи
};

// типы платежей
export const PAYMENT_TYPES = {
  PAYMENT: "payment", // оплата покупки
  REWARD: "reward", // выплата вознаграждения
  SUPPLY: "supply", // оплата поставки
  CERTIFICATE: "certificate", // оплата сертификата
  RETURN: "return", // оплата возврата
};

// методы оплаты
export const PAYMENT_METHODS = {
  YANDEX: "yandex",
  MANUAL: "manual",
};

// статусы платежей
export const PAYMENT_STATUSES = {
  NEW: "new", // новый
  WAIT_PAYMENT: "wait_payment", // ожидает оплаты
  PAID: "paid", // оплачен
  DECLINED: "declined", // отменен
  INCOME: "income", // доход
};

// статусы заказов
export const ORDER_STATUSES = {
  DRAFT: "draft", // черновик
  NEW: "new", // новый
  PAID: "paid", // оплачен
  PACKED: "packed", // упакован
  TRANSFER: "transfer", // передан в доставку
  COMPLETED: "completed", // завершен
  DECLINED: "declined", // отменен
};

// статусы поставок
export const SUPPLY_STATUSES = {
  DRAFT: "draft", // черновик
  IN_TRANSFER: "in_transfer", // доставляется
  NEW: "new", // новая
  IN_PROGRESS: "in_progress", // выполняется
  CHECKING: "checking", // проверка
  COMPLETED: "completed", // завершенная
  DECLINED: "declined", // отмененная
};

// статусы сертификатов
export const CERTIFICATE_STATUSES = {
  REQUESTED: "requested",
  ACTIVE: "active",
  EXPIRED: "expired",
  USED: "used",
  ERROR: "error",
};

// статусы адресов
export const ADDRESS_STATUSES = {
  NEW: "new", // новый
  SUCCESS: "success", // успешный
  ERROR: "error", // ошибочный
};

// типы партнеров
export const PARTNER_TYPES = {
  GIFTERY: "giftery",
};

// типы транспортных компаний
export const TRANSPORT_TYPES = {
  OFFICE: "office",
  BOXBERRY: "boxberry",
  AVITO: "avito",
  DOSTAVISTA: "dostavista",
  CDEK: "cdek",
};

// типы стоимостей
export const COST_TYPES = {
  SUPER_ECONOMY: "super_economy", // супер-эконом класс
  ECONOMY: "economy", // эконом класс
  MASS: "mass", // масс-маркет
  MIDDLE: "middle", // средний класс
  LUX: "lux", // люксовый
};

// типы промокодов
export const PROMOCODE_TYPES = {
  PUBLIC: "public", // публичный промокод
  PERSONAL: "personal", // персональный промокод, может быть связан с конкретным пользователем
};

// ошибки промокодов
export const PROMOCODE_ERRORS = {
  NOT_FOUND: "promocode_not_found", // промокод не найден
  ALREADY_SET: "promocode_already_set", // промокод уже установлен
  ALREADY_USED: "promocode_already_used", // промокод уже использован
  NOT_ACTIVE: "promocode_not_active", // промокод еще не активен
  EXPIRED: "promocode_expired", // истекло время активности
  NOT_ALLOWED: "promocode_not_allowed", // промокод недоступен
};

// типы признаков расчета в чеках
export const RECEIPT_TYPES = {
  SUPPLY_ITEMS: "supply_items", // обмен вещей на встречное предоставление
  SUPPLY_REWARD: "supply_reward", // обмен встречного предоставления на опцион
  ORDER_PAID: "order_paid", // оплата заказа (внесение денег с карты в качестве аванса)
  ORDER_SUCCESS: "order_success", // завершение заказа (оплата авансом и встречным предоставлением)
  PRODUCT_RETURN_INCOME: "product_return_income", // возврат предмета из заказа
  SHIPMENT_PAID: "shipment_paid", // оплата доставки
  SHIPMENT_RETURN_INCOME: "shipment_return_income", // возврат денег за оплаченную доставку
  CERTIFICATE_SUCCESS: "certificate_success", // выпуск сертификата
};

// статусы чеков
export const RECEIPT_STATUSES = {
  NEW: "new", // новый
  READY: "ready", // готов к отправке
  PROCESS: "process", // обрабатывается
  SUCCESS: "success", // готов
  ERROR: "error", // ошибка
};

// типы категорий товаров
export const CATEGORIES_TYPES = {
  CLOCLO: "cloclo",
  AVITO: "avito",
  YANDEX: "yandex",
  VK: "vk",
};

// сервисы - источники данных для отзывов
export const FEEDBACK_SERVICES = {
  SITE: "site",
  AVITO: "avito",
  FACEBOOK: "facebook",
  INSTAGRAM: "instagram",
  WHATSAPP: "whatsapp",
  VK: "vk",
  YANDEX: "yandex",
  DEPOP: "depop",
  EBAY: "ebay",
};

// типы скидок
export const SALES_TYPES = {
  MINUS_PRICE: "minus_price",
  MINUS_PERCENT: "minus_percent",
  FIX_PRICE: "fix_price",
};

// позиции для скидок
export const SALES_POSITIONS = {
  PRODUCT: "product", // товар
  TOTAL: "total", // все содержимое
};

// критерии для скидок
export const SALES_CRITERIAS = {
  EQ: "eq", // равно
  GTE: "gte", // больше или равно
  LTE: "lte", // меньше или равно
};

// сегментация контекста данных
export const CONTEXT_SEGMENTS = {
  DEFAULT: null, // для по умолчанию - NULL
  ADULT: "adult", // для взрослых
  KIDS: "kids", // для детей
};

// возможные значения полов
export const GENDERS = {
  FEMALE: "female",
  MALE: "male",
};

// части одежды для размеров (одежда/обувь)
export const SIZES_PARTS = {
  DRESS: "dress",
  SHOES: "shoes",
};

// виды мерок (длина, высота, талия, бедра и т.д.)
export const SIZES_PROPS = {
  TALL: "tall",
  LENGTH: "length",
  BREAST: "breast",
  WAIST: "waist",
  HIPS: "hips",
  SHOULDERS: "shoulders",
  SLEEVE: "sleeve",
  SOCK: "sock",
  HEEL: "heel",
  HEIGHT: "height",
  WIDTH: "width",
};
